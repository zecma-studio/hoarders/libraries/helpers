﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Helpers.Config
{
    public class ClientSettings
    {
        public string Zecmail { get; set; }
        public string Zeclogin { get; set; }
    }
}
