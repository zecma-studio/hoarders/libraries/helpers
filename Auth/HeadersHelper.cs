﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace Helpers.Auth
{
    public class HeadersHelper
    {
        public static int  GetIdFromAuth(HttpContext httpContext)
        {
            try
            {
                var identity = httpContext.User.Identity as ClaimsIdentity;
                int userId;
                if (identity != null)
                {
                    userId = Int32.Parse(identity.FindFirst("userId").Value);
                }
                return 1;
            }catch(Exception e)
            {
                return 0;
            }
        }
    }
}
