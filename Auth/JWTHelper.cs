﻿using System;
using System.Security.Claims;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using Entities.Zeclogin;
using Microsoft.IdentityModel.Tokens;

namespace Helpers.Auth
{
    public class JWTHelper
    {
        //TODO: Move JWT to initialize from ENV or appsettings.json
        private const string JWTKey = "ZECMASTUDIO6YEARS";


        //Todo: Make this part Abstract
        public static string ConvertUserToJWT(User user)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.UTF8.GetBytes(JWTKey);
            Console.WriteLine("In Encrypt " + JWTKey);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]{
                    //TODO: Modify needed data for encrypting
                    new Claim("email", user.Email),
                    new Claim("userId", user.Id+""),//Fix this
                    new Claim(ClaimTypes.Role, user.Role),
                }),
                Expires = DateTime.UtcNow.AddDays(4),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };
            //Create token
            var token = tokenHandler.CreateToken(tokenDescriptor);
            string jwtToken = tokenHandler.WriteToken(token);
            return jwtToken;
        }
        
        public static object CheckJWT(string jwt)
        {
            try
            {

                byte[] key = Encoding.UTF8.GetBytes(JWTKey);
                TokenValidationParameters validationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
                SecurityToken validatedToken;
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                var user = handler.ValidateToken(jwt, validationParameters, out validatedToken);

                //TODO: Make this object return something abstract
                if (user.Claims.Count() == 0)
                    return null;
                var values = toKeyValuePairs(user.Claims);
                return values;
            }
            catch (Exception e)
            {

                //TODO: add handle exceptions
                return null;
            }
        }

        public static Dictionary<String, Claim> toKeyValuePairs(IEnumerable<Claim> claims)
        {
            Dictionary<String, Claim> dicClaim = claims
                .GroupBy(claim => claim.Type)
                .SelectMany(group => group
                    .Select((item, index) => group.Count() <= 1
                           ? Tuple.Create(group.Key, item)
                           : Tuple.Create($"{group.Key}_{index + 1}", item)
                           ))
                .ToDictionary(tuple => tuple.Item1, tuple => tuple.Item2);
            return dicClaim;
        }

        public static string ConvertUserToUrlJwt(User user)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.UTF8.GetBytes(JWTKey);
            Console.WriteLine("In Encrypt " + JWTKey);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]{
                    //TODO: Modify needed data for encrypting
                    new Claim("email", user.Email),
                    new Claim("userId", user.Id+""),//Fix this
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            string jwtToken = tokenHandler.WriteToken(token);
            return jwtToken;
        }

    }
}
