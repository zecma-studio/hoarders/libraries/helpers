using System.Globalization;
using System.IO;
using System.Collections.Generic;
using CsvHelper;
using System.Linq;

namespace Helpers.Core
{
    public class CountryHelper
    {
        public static string SearchCountry(string code)
        {
            TextReader reader = new StreamReader("countries.csv");
            var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
            var records = csvReader.GetRecords<Country>();
            records.First<Country>(country => country.Code == code);
            return (code != null ? code : "");
        }
        /*
        This method compares a string given with the csv of countries. Return true if all countries are found, false if not

        */
        public static bool SearchCountryByString(string codesOnString)
        {
            string[] codes = codesOnString.Split(new string[] { "," }, System.StringSplitOptions.RemoveEmptyEntries); 
            TextReader reader = new StreamReader("countries.csv");
            var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
            var records = csvReader.GetRecords<Country>();
            var countries =0;
            foreach(var code in codes){
                if(records.First(coun=> coun.Code==code)!=null)
                    countries++;
            }
            return (countries == codes.Length ? true : false);
        }
        /*
            Return @records the country list from countries.csv
        */
        public static List<Country> getCountryList(){
            TextReader reader = new StreamReader("countries.csv");
            var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
            var records = csvReader.GetRecords<Country>();
            return records.ToList();
        }
    }
}